$('.fa-bars').on('click', function () {
    $('.fa-times').css('display', 'inline-block')
    $('.fa-bars').css('display', 'none')
    $('.navList').css('display','inline-block')
})

$('.fa-times').on('click', function () {
    $('.fa-times').css('display', 'none')
    $('.fa-bars').css('display', 'inline-block')
    $('.navList').css('display','none')
})

